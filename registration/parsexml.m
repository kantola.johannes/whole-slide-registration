function [ rowStart rowEnd colStart colEnd ] = parsexml( baseFileName )
%% baseFileName='KYS1_hamamatsu.ndpi';
[pathstr,name,ext]=fileparts(baseFileName);
filename=[name '.xml']
if exist(filename, 'file')
    s=xml2struct(filename);
    regioncount=size(s.Annotations.Annotation.Regions.Region,2)
    if regioncount>1
        k1X=s.Annotations.Annotation.Regions.Region{1,1}.Vertices.Vertex{1,1}.Attributes.X;
        k2X=s.Annotations.Annotation.Regions.Region{1,1}.Vertices.Vertex{1,2}.Attributes.X;
        k1Y=s.Annotations.Annotation.Regions.Region{1,1}.Vertices.Vertex{1,1}.Attributes.Y;
        k3Y=s.Annotations.Annotation.Regions.Region{1,1}.Vertices.Vertex{1,3}.Attributes.Y;
    else
        k1X=s.Annotations.Annotation.Regions.Region.Vertices.Vertex{1,1}.Attributes.X;
        k2X=s.Annotations.Annotation.Regions.Region.Vertices.Vertex{1,2}.Attributes.X;
        k1Y=s.Annotations.Annotation.Regions.Region.Vertices.Vertex{1,1}.Attributes.Y;
    k3Y=s.Annotations.Annotation.Regions.Region.Vertices.Vertex{1,3}.Attributes.Y;
    end
    colStart=round(str2num(k1X));
    colEnd=round(str2num(k2X));
    rowStart=round(str2num(k1Y));
    rowEnd=round(str2num(k3Y))
else colStart=0;
    colEnd=0;
    rowStart=0;
    rowEnd=0;
end

