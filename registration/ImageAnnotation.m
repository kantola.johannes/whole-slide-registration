function [rgbImage1]=ImageAnnotation(levelNDPI1,levelSVS1,path1)
% rowStartD=16000;
% rowEndD=17000;
% colStartD=5000;
% colEndD=6000;
rowStartD=6300;
rowEndD=7300;
colStartD=12000;
colEndD=13000;

workspace;      % Make sure the workspace panel is showing.
fontSize = 12;

%% Construct the folder name where the  images live.
imagesFolder1 =path1
% Create output folder if not exist
outputFolder1 = [path1 '\Output'];
%% Create folder if it doesn't exist
if ~exist(outputFolder1, 'dir')
    mkdir(outputFolder1);
end
%% Create title line for results
OutputFile1=[outputFolder1 '\' datestr(now,30) 'Results1.txt']
fid1=fopen(OutputFile1,'at') 
fprintf(fid1, 'Filename      Stainvector  \n');
%% Read the directory to get a list of images.
filePattern1 = [imagesFolder1, '\*.svs'];
svsFiles1 = dir(filePattern1);
filePattern1 = [imagesFolder1, '\*.ndpi'];
ndpiFiles1 = dir(filePattern1);
filePattern1 = [imagesFolder1, '\*.tif'];
tifFiles1 = dir(filePattern1);
%% Add more extensions if you need to.
% imageFiles = [jpegFiles; tifFiles; pngFiles; bmpFiles];
imageFiles1=[svsFiles1; ndpiFiles1; tifFiles1]
%% Bail out if there aren't any images in that folder.
numberOfImagesProcessed1 = 0;
numberOfImagesToProcess1 = length(imageFiles1);
if numberOfImagesToProcess1 <= 0
 	message1 = sprintf('No images in the folder\n%s\nClick OK to Exit.', imagesFolder1);
	uiwait(msgbox(message1));
	return;
end

%% OTHER VALUES DESCRIBING THE DATA
numberOfImagesToProcess2 = numberOfImagesToProcess1;

% Loop though all images, calculating and displaying the histograms.
% And then getting the means of the Red, green, and blue channels.
for k = 1 : numberOfImagesToProcess1
% Read in this one file.
% convType= 'From_control'
	baseFileName1 = imageFiles1(k).name;
    if (contains(baseFileName1,'oys'))
        cases(k)=1;
    % convType= 'From_control'
    elseif (contains(baseFileName1,'KYS'))
        cases(k)=2;
    % convType='KYS';
    elseif (contains(baseFileName1,'oys'))&&(contains(baseFileName1,'ndpi'))
        cases(k)=3;
    else cases(k)=4;
    end
	fullFileName1 = fullfile(imagesFolder1, baseFileName1);
    % Reading the file with lower resolution
    if (contains(baseFileName1,'.svs'))
        if levelSVS1==1
           [ rowStart1 rowEnd1 colStart1 colEnd1 ] = parsexml( baseFileName1 );
           if rowStart1==0
           rgbImage1 = imread(fullFileName1,'Index',levelSVS1,'PixelRegion',{[rowStartD,rowEndD],[colStartD,colEndD]});
           % rgbImage = imread(fullFileName,'Index',levelSVS); 
           else
           rgbImage1 = imread(fullFileName1,'Index',levelSVS1,'PixelRegion',{[rowStart1,rowEnd1],[colStart1,colEnd1]});
           end
        else
           rgbImage1 = imread(fullFileName1,'Index',levelSVS1);
        end
        ThumbImage1 = imread(fullFileName1,'Index',2);
        HE_evalImage1 = imread(fullFileName1,'Index',4);
    elseif (contains(baseFileName1,'.ndpi'))
        if levelNDPI1==1
%% Check if a conversion file exits 
    [pathstr,name,ext]=fileparts(baseFileName1);
        filename=[name '.tif']
     if exist(filename, 'file')==2
        [ rowStart1 rowEnd1 colStart1 colEnd1 ] = parsexml( baseFileName1 );
        if rowStart1==0
          rgbImage1 = imread(fullFileName1,'Index',levelNDPI1,'PixelRegion',{[rowStartD,rowEndD],[colStartD,colEndD]});
        else
        % rgbImage = imread(fullFileName,'Index',levelNDPI,'PixelRegion',{[rowStart,rowEnd],[rowStart,rowEnd]});
          rgbImage1 = imread(filename,'PixelRegion',{[rowStart1,rowEnd1],[colStart1,colEnd1]});
        end
        else
          rgbImage1 = imread(fullFileName1,'Index',levelNDPI1+1,'PixelRegion',{[rowStartD,rowEndD],[colStartD,colEndD]});
        end
        else 
          rgbImage1 = imread(fullFileName1,'Index',levelNDPI1);
        end
        ThumbImage1 = imread(fullFileName1,'Index',4);
        HE_evalImage1 = imread(fullFileName1,'Index',3);
        elseif (contains(baseFileName1,'.tif'))
            rgbImage1 = imread(fullFileName1);
            ThumbImage1 = rgbImage1;
            HE_evalImage1 =imresize(rgbImage1,[5000 5000]);
    end
end
end
