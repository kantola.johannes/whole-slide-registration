test_img = imread('C:\code\bme_project\Images\Images\testimage\arrow.png');
test_img_rot = imrotate(test_img, -30, 'crop');
test_img = rgb2gray(test_img);
test_img_rot = rgb2gray(test_img_rot);

f = @(r) corr2(test_img, imrotate(test_img_rot, r, 'crop'));
fd = @(prev_f, curr_f, prev_x, curr_x) ((prev_f - curr_f) / (prev_x - curr_x));

alpha = 0.1; % Learning rate
max_iter = 100;
thresh = 0.0001; % Stop condition

prev_x = 0;
prev_y = f(prev_x);
x = 10; % Start point
y = f(x);

diff = thresh + x;

iter = 0;
while(diff > thresh && iter < max_iter)
    x = x - alpha * fd(prev_y,y,prev_x,x);
    y = f(x);
    iter = iter+1;
    diff = abs(prev_y-y);
    prev_x = x;
    prev_y = y;
end

% r0 = 10;
% newton(f, r0, 0.00001)
% 
% function [result] = newton(f, r0, thres)
% 
% fd = @(prev_f, curr_f, prev_x, curr_x) ((prev_f - curr_f) / (prev_x - curr_x));
% 
% prev_e = f(0);
% prev_r = 0;
% r = r0;
% prev_de = fd(f(0), f(r0), 0, r0);
% de = r0;
% 
% while(abs(de) > thres)
%     e = f(r);
%     de = fd(prev_e,e,prev_r,r);
%     dde = fd(prev_de,de,prev_r,r);
%     prev_e = e;
%     prev_r = r;
%     prev_de = de;
%     
%     r = r - de/dde;
% end
% 
% result = r;
% 
% end

