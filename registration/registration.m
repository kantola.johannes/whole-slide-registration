function [outputImages, results] = registration(image, rows, cols, morph_close, morph_erode)

%% REGISTRATION Image registration for .svs whole slide images
%   Johannes Kantola


%% Detect samples

[objects, centroids] = detectSamples(image, rows, cols, morph_close, morph_erode);


%% Transpose Images by Padding
results = {};
outputImages = cell(cols, rows);
for i = 1:size(objects,2)
    ref = objects{1,i};
    outputImages{1,i} = ref;
    for j = 2:size(objects,1)
        tic
        sample = objects{j,i};
        if centroids{1,i}(1) > centroids{j,i}(1)
            sample = padarray(sample, [0 round(centroids{1,i}(1) - centroids{j,i}(1))],'pre');
        else
            ref = padarray(ref, [0 round(centroids{j,i}(1) - centroids{1,i}(1))],'pre');
        end
        if size(ref,2) > size(sample,2)
            sample = padarray(sample, [0 size(ref,2) - size(sample,2)],'post');
        else
            ref = padarray(ref, [0 size(sample,2) - size(ref,2)],'post');
        end
        
        if centroids{1,i}(2) > centroids{j,i}(2)
            sample = padarray(sample, round(centroids{1,i}(2) - centroids{j,i}(2)),'pre');
        else
            ref = padarray(ref, round(centroids{j,i}(2) - centroids{1,i}(2)),'pre');
        end
        if size(ref,1) > size(sample,1)
            sample = padarray(sample, size(ref,1) - size(sample,1),'post');
        else
            ref = padarray(ref, size(sample,1) - size(ref,1),'post');
        end

        % Calculate correlation etc. here
        result = {};
%         result.correlation = corr2(ref, sample);
%         result.SSIM = ssim_index(ref, sample);
%         result.MSSIM = FR_MSSIM(ref, sample);
%         result.FSIM = FR_FSIMc(ref, sample);
%         result.RMSE = FR_RMSE(ref, sample)
        results{end+1} = result;
        
        outputImages{j,i}.reference = ref;
        outputImages{j,i}.result = sample;
        toc
    end
end

end

