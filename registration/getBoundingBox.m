function [output] = getBoundingBox(inputImage, whiteThresh, adaptThresh, close)
%GETBOUNDINGBOX Finds and returns the bounding box of an object in an image
%   Finds and returns the bounding box of the largest object that can be
%   registered from an input image.

inputImageWithoutBlack = inputImage;
inputImageWithoutBlack(inputImage < whiteThresh) = 255;
inputProcessed = imadjust(inputImageWithoutBlack);

imageAdaptThresh = adaptthresh(inputProcessed, adaptThresh, 'ForegroundPolarity', 'Dark', 'Statistic', 'gaussian');
imageThresh = imbinarize(inputProcessed, imageAdaptThresh);

bin = imcomplement(imageThresh);

se = strel('disk', close);
bin = imclose(bin, se);

regprops = regionprops(bin, 'basic');
[~, idx] = max([regprops.Area]);
bb = regprops(idx).BoundingBox;

output = bb;

end

