close all;
clear;

rows = 2;
cols = 2;
morph_close = 23;
morph_erode = 4;

scale = 0.2;

image = ImageAnnotation(4,3,'C:\code\bme_project\Images\Images\Image2');
image = imresize(image, scale);

%% Proposed method

[images, results] = registration(image, rows, cols, morph_close, morph_erode);
% image = imread('C:\code\bme_project\Images\Images\testimage\brain.png');
% [images, results] = registration(image, rows, cols, morph_close, morhp_erode);

ref = rgb2gray(images{2,1}.reference);
res = rgb2gray(images{2,1}.result);

stats_proposed = similarities(ref, res)

figure 
imshow(imfuse(images{2,1}.reference, images{2,1}.result, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]));
title('Proposed Method Results');

%%
% image1 = imread('C:\code\bme_project\Images\Images\slide2_1.tif');
% image2 = imread('C:\code\bme_project\Images\Images\slide2_2.tif');
[images, ~] = detectSamples(image, rows, cols, morph_close, morph_erode);
image1 = images{1,1};
image2 = images{2,1};
image1_processed = imadjust(rgb2gray(image1));
image2_processed = imadjust(rgb2gray(image2));


%% SIFT

tic
points1 = detectSIFTFeatures(image1_processed);
points2 = detectSIFTFeatures(image2_processed);
[features1, validPoints1] = extractFeatures(image1_processed, points1);
[features2, validPoints2] = extractFeatures(image2_processed, points2);

indexPairs = matchFeatures(features1,features2);
matchedPoints1 = validPoints1(indexPairs(:,1));
matchedPoints2 = validPoints2(indexPairs(:,2));

[tform,inlierPoints1,inlierPoints2] = estimateGeometricTransform(matchedPoints2,matchedPoints1,'similarity');
%figure; showMatchedFeatures(image1,image2,inlierPoints1,inlierPoints2);

outputView = imref2d(size(image1_processed));
res = imwarp(image2,tform,'OutputView',outputView);
toc

stats_SIFT = similarities(rgb2gray(image1), rgb2gray(res))

figure 
imshow(imfuse(stats_SIFT.ref, stats_SIFT.res, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]));
title('SIFT Results');


%% SURF

tic
points1 = detectSURFFeatures(image1_processed);
points2 = detectSURFFeatures(image2_processed);
[features1, validPoints1] = extractFeatures(image1_processed, points1);
[features2, validPoints2] = extractFeatures(image2_processed, points2);

indexPairs = matchFeatures(features1,features2);
matchedPoints1 = validPoints1(indexPairs(:,1));
matchedPoints2 = validPoints2(indexPairs(:,2));

[tform,inlierPoints1,inlierPoints2] = estimateGeometricTransform(matchedPoints2,matchedPoints1,'similarity');
%figure; showMatchedFeatures(image1,image2,inlierPoints1,inlierPoints2);

outputView = imref2d(size(image1_processed));
res = imwarp(image2,tform,'OutputView',outputView);
toc

stats_SURF = similarities(rgb2gray(image1), rgb2gray(res))

figure 
imshow(imfuse(stats_SURF.ref, stats_SURF.res, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]));
title('SURF Results');


%% ORB

tic
points1 = detectORBFeatures(image1_processed);
points2 = detectORBFeatures(image2_processed);
points1 = selectStrongest(points1, 100000);
points2 = selectStrongest(points2, 100000);

[features1, validPoints1] = extractFeatures(image1_processed, points1);
[features2, validPoints2] = extractFeatures(image2_processed, points2);

indexPairs = matchFeatures(features1,features2,'Method','Approximate');
matchedPoints1 = validPoints1(indexPairs(:,1));
matchedPoints2 = validPoints2(indexPairs(:,2));

[tform,inlierPoints1,inlierPoints2] = estimateGeometricTransform(matchedPoints2,matchedPoints1,'similarity');
%figure; showMatchedFeatures(image1,image2,inlierPoints1,inlierPoints2);

outputView = imref2d(size(image1_processed));
res = imwarp(image2,tform,'OutputView',outputView);
toc

stats_ORB = similarities(rgb2gray(image1), rgb2gray(res))

figure 
imshow(imfuse(stats_ORB.ref, stats_ORB.res, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]));
title('ORB Results');


%% BRISK

tic
points1 = detectBRISKFeatures(image1_processed);
points2 = detectBRISKFeatures(image2_processed);
[features1, validPoints1] = extractFeatures(image1_processed, points1);
[features2, validPoints2] = extractFeatures(image2_processed, points2);

indexPairs = matchFeatures(features1,features2);
matchedPoints1 = validPoints1(indexPairs(:,1));
matchedPoints2 = validPoints2(indexPairs(:,2));

[tform,inlierPoints1,inlierPoints2] = estimateGeometricTransform(matchedPoints2,matchedPoints1,'similarity');
%figure; showMatchedFeatures(image1,image2,inlierPoints1,inlierPoints2);

outputView = imref2d(size(image1_processed));
res = imwarp(image2,tform,'OutputView',outputView);
toc

stats_BRISK = similarities(rgb2gray(image1), rgb2gray(res))

figure 
imshow(imfuse(stats_BRISK.ref, stats_BRISK.res, 'falsecolor','Scaling','joint','ColorChannels',[1 2 0]));
title('BRISK Results');