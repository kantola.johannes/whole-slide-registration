function [results] = similarities(stationaryImage,movingImage)
%SIMILARITIES Returns typical measurements of similarity between two grayscale slice images 
%   The similarities are measured between the images within a bounding box
%   that includes both of the slices. This leaves out any unnecessary data.
%   The images have to be in grayscale.

% Get the bounding boxes of the images
bb1 = getBoundingBox(stationaryImage,120,0.5,5);
bb2 = getBoundingBox(movingImage,120,0.5,5);

% Get the smallest possible bounding box that includes both samples
% completely but leaves everyghing else out
bb = [min([bb1(1) bb2(1)]),min([bb1(2) bb2(2)]),max([bb1(3) bb2(3)]),max([bb1(4) bb2(4)])];
bb = uint16(bb);

ref = imcrop(stationaryImage, bb);
res = imcrop(movingImage, bb);

results.correlation = corr2(ref, res);
results.SSIM = ssim_index(ref, res);
results.MSSIM = FR_MSSIM(ref, res);
results.FSIM = FR_FSIMc(ref, res);
results.RMSE = FR_RMSE(ref, res);
results.ref = ref;
results.res = res;

end

