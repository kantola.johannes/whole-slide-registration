%image = imread(".\test_image.png");
image = ImageAnnotation(4,3,'C:\code\bme_project\Images\Images\Image2');
[images, results] = registration(image, 1, 2, 60, 20);
% image = imread('C:\code\bme_project\Images\Images\testimage\brain.png');
% [images, results] = registration(image, 2, 2, 23, 4);
imshow(images{1});
figure;
imshow(images{2});

% ref = images{1,1};
% res = images{2,1};
% points1 = detectSURFFeatures(ref);
% points2 = detectSURFFeatures(res);
% [f1, vpts1] = extractFeatures(ref, points1);
% [f2, vpts2] = extractFeatures(ref, points2);
% 
% indexPairs = matchFeatures(f1, f2);
% matchedPoints1 = vpts1(indexPairs(:,1));
% matchedPoints2 = vpts2(indexPairs(:,2));
% 
% figure; showMatchedFeatures(ref,res,matchedPoints1,matchedPoints2);
% legend('matched points 1','matched points 2');