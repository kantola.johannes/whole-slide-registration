function [samples,centroids] = detectSamples(inputImage, rows, cols, morph_close, morph_erode)
%DETECTSAMPLES Detect samples from an image of multiple samples

%% Threshold Image with Global Thresholding

% Adaptive thresholding with Otsu's method
image = rgb2gray(inputImage);
image_threshold = adaptthresh(image, 0.55, 'ForegroundPolarity', 'Dark');
bin = imbinarize(image, image_threshold);
bin = imcomplement(bin);

% Morphological operations
se = strel('disk', morph_close);
bin = imclose(bin, se);
se = strel('disk', morph_erode);
bin = imerode(bin, se);


%% Label Image, Pick and Order Tissue Components

% Get all islands in the image
[labels, n_of_objects] = bwlabel(bin, 4);

% Get n=rows*cols largest islands to filter debris
limits = {};
for i = 1:n_of_objects
   [row, column] = find(labels==i);
   limits{end+1} = [row, column];
end

[ncols, ~] = cellfun(@size, limits);
[~, order] = sort(ncols, 2);

samples = limits(order);
samples = samples(end-rows*cols+1:end);

% Sort samples by row
ncols = zeros(1,size(samples,2));
for i = 1:size(samples,2)
    ncols(i) = min(samples{i}(:,1));
end

[~, order] = sort(ncols, 'ascend');
samples = samples(order);

stacks = cell(cols, rows);
for i = 1:size(samples,2)
    stacks{mod(i-1,cols)+1, floor((i-1)/cols)+1} = samples{i};
end

% Sort samples by column
for i = 1:size(stacks,2)
    ncols = [];
    for j = 1:size(stacks,1)
        ncols(end+1) = min(stacks{j,i}(:,2));
    end
    [~, order] = sort(ncols, 'ascend');
    currStack = stacks(:,i);
    stacks(:,i) = currStack(order');
end

% Get filtered samples and their centroids
samples = cell(cols, rows);
centroids = cell(cols, rows);
for i = 1:size(stacks,2)
    for j = 1:size(stacks,1)
        row = stacks{j, i}(:,1);
        column = stacks{j, i}(:,2);
        samples{j,i} = inputImage(min(row):max(row), column(1):column(end), :);
        bw = bin(min(row):max(row), column(1):column(end));
        regprops = regionprops(bw, 'centroid');
        centroids{j,i} = regprops.Centroid;
    end
end

end

